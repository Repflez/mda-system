#==============================================================================
# ** MakerCrypt by Repflez (version 0.1)
#------------------------------------------------------------------------------
# MakerCrypt is an easy "encryption" system that can be used for an easy way
# to obfuscate files to prying eyes and novice users.
#
# See the README file in the repo for usage.
#==============================================================================

module MakerCrypt
  #--------------------------------------------------------------------------
  # * Do the encryption
  #--------------------------------------------------------------------------
  def self.encrypt(filename)
    File.rename(filename, filename + '_temp')
    file = File.open(filename + '_temp', 'rb')    
    rawdata = file.read
    file.close
    first = nil
    rawdata.each_byte {|byte|
      first = byte
    break}
    data = Zlib::Deflate.deflate(rawdata)
    file = File.open(filename, 'wb')
    file.write(data)
    file.close
    File.delete(filename + '_temp')
  end
  #--------------------------------------------------------------------------
  # * Decrypt
  #--------------------------------------------------------------------------
  def self.decrypt(filename)
    file = File.open(filename, 'rb')    
    rawdata = file.read
    file.close
    first = nil
    rawdata.each_byte {|byte|
      first = byte
    break}
    data = Zlib::Inflate.inflate(rawdata)
    file = File.open(filename, 'wb')    
    file.write(data)
    file.close
  end
end