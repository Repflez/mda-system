#==============================================================================
# ** OSave by Repflez (version 0.2)
#------------------------------------------------------------------------------
# OSave is a script that allows a game to store their files in the user
# folders such as %APPDATA% or %LOCALAPPDATA% depending of the OS used.
#
# See the README file in the repo for usage.
#==============================================================================

module OSave
  # Folder path. Try to use foward slashes in the path.
  Folder_Path = '/My Awesome Company/My Awesome Game/'
  # File mode. false for the Production folder and true for the Debug folder
  Debug_Folder = true
  # Create separate folders for each user in the computer?
  User_Folder = true
  # Create data in the game folder or in the local folder?
  Local_Folder = false
  #--------------------------------------------------------------------------
  # * Setup Game Data Folder
  #--------------------------------------------------------------------------
  def self.setup_userdata_folder
    path = ENV['ALLUSERSPROFILE'].gsub('\\') {'/'}
    path += '/' + ENV['APPDATA'].split('\\').pop if ENV['LOCALAPPDATA'] == nil
    path += Folder_Path
    path += ENV['USERNAME'] + '/' if User_Folder
    path += 'Debug/' if Debug_Folder
    return path
  end
  #--------------------------------------------------------------------------
  # * Setup Saves Folder
  #--------------------------------------------------------------------------
  def self.setup_saves_folder
    begin
      @saves = self.setup_userdata_folder + 'Saves/'
      self.create_folder(@saves)
    rescue
      @saves = 'Saves/'
      self.create_folder(@saves)
    end
  end
  #--------------------------------------------------------------------------
  # * Create Folders
  #--------------------------------------------------------------------------
  def self.create_folder(path)
    folders = path.split('/')
    path = folders.shift
    folders.each {|folder|
        path += '/' + folder
        Dir.mkdir(path) if !FileTest.exist?(path)}
  end
  #--------------------------------------------------------------------------
  # * Saves
  #--------------------------------------------------------------------------
  def self.saves
    return '' if Local_Folder
    create_folder(@saves)
    return @saves
  end
end